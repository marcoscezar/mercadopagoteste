package razecapps.pagamentomercadopagoteste.config

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class PaidMarketGlideApp : AppGlideModule()