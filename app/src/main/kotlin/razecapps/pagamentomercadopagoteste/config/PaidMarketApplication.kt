package razecapps.pagamentomercadopagoteste.config

import android.app.Application
import org.koin.android.ext.android.startKoin
import razecapps.pagamentomercadopagoteste.BuildConfig
import razecapps.pagamentomercadopagoteste.di.mainModule
import razecapps.pagamentomercadopagoteste.di.paidMarketRemoteModule
import timber.log.Timber
import timber.log.Timber.DebugTree

class PaidMarketApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(mainModule, paidMarketRemoteModule))

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }

}