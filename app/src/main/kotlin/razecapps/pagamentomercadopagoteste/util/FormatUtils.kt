package razecapps.pagamentomercadopagoteste.util

import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

fun String.parseFromDollar(): Number {
    val moneyBrFormater = NumberFormat.getCurrencyInstance(Locale("pt", "BR"))
    return moneyBrFormater.parse(this)
}

fun BigDecimal.formatToDollar(): String {
    val moneyBrFormater = NumberFormat.getCurrencyInstance(Locale("pt", "BR"))
    return moneyBrFormater.format(this)
}