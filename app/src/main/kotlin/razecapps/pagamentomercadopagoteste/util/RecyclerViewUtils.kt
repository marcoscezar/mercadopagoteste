package razecapps.pagamentomercadopagoteste.util

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

fun RecyclerView
        .createLinearRecyclerView(context: Context,
                                  adapter: RecyclerView.Adapter<out RecyclerView.ViewHolder>) {
    this.setHasFixedSize(true)
    this.layoutManager = LinearLayoutManager(context)
    this.adapter = adapter
}