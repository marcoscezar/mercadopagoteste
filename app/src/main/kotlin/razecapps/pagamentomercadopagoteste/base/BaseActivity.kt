package razecapps.pagamentomercadopagoteste.base

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.view.MenuItem
import android.widget.TextView
import razecapps.pagamentomercadopagoteste.R

abstract class BaseActivity : AppCompatActivity() {

    companion object {
        const val CLOSE_INTERMEDIARY_RECEIVER = "razecapps.closeIntermediaryReceiver"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    protected fun setupToolbarTitle(toolbar: Toolbar?, title: String) {

        val toolbarTitle = toolbar?.findViewById<TextView>(R.id.textMainToolbarTitle)

        if (title.isNotEmpty()) {
            toolbarTitle?.text = SpannableStringBuilder.valueOf(title)
        }

        setSupportActionBar(toolbar)

        supportActionBar?.apply {
            this.setDisplayShowTitleEnabled(false)
        }

    }

    protected fun configureToolbarBackButton() {
        supportActionBar?.apply {
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return false
    }
}