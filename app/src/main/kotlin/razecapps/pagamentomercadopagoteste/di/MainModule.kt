package razecapps.pagamentomercadopagoteste.di

import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import razecapps.pagamentomercadopagoteste.ui.presenter.MainContract
import razecapps.pagamentomercadopagoteste.ui.presenter.MainPresenter


val mainModule: Module = applicationContext {

    factory {
        MainPresenter() as MainContract.MainPresenter<MainContract.MainView>
    }

}