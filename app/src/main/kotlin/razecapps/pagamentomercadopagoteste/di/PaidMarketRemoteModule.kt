package razecapps.pagamentomercadopagoteste.di

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import razecapps.pagamentomercadopagoteste.BuildConfig
import razecapps.pagamentomercadopagoteste.repository.PaidMarketRepository
import razecapps.pagamentomercadopagoteste.ui.presenter.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val paidMarketRemoteModule: Module = applicationContext {

    val ioScheduler = "ioScheduler"
    val uiScheduler = "uiScheduler"

    factory {
        SelectPaymentPresenter(get(), get(ioScheduler), get(uiScheduler)) as SelectPaymentContract
        .SelectPaymentPresenter<SelectPaymentContract.SelectPaymentMethodView>
    }

    factory {
        BanksPresenter(get(), get(ioScheduler), get(uiScheduler)) as BanksContract.BanksPresenter<BanksContract.BanksView>
    }

    factory {
        InstallmentsPresenter(get(), get(ioScheduler), get(uiScheduler)) as
                InstallmentsContract.InstallmentsPresenter<InstallmentsContract.InstallmentsView>
    }

    bean(ioScheduler) {
        Schedulers.io()
    }

    bean(uiScheduler) {
        AndroidSchedulers.mainThread()
    }

    bean {
        createWebService<PaidMarketRepository>(get(), BuildConfig.PAID_MARKET_BASE_URL)
    }

    bean {
        createOkHttpClient()
    }

}

fun createOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return OkHttpClient.Builder()
            .connectTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor).build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}