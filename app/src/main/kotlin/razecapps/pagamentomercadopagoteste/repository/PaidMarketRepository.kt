package razecapps.pagamentomercadopagoteste.repository

import io.reactivex.Observable
import razecapps.pagamentomercadopagoteste.BuildConfig
import razecapps.pagamentomercadopagoteste.repository.json.Bank
import razecapps.pagamentomercadopagoteste.repository.json.InstallmentsResponse
import razecapps.pagamentomercadopagoteste.repository.json.PaymentMethodResponse
import retrofit2.http.GET
import retrofit2.http.Query
import java.math.BigDecimal

interface PaidMarketRepository {

    @GET("payment_methods?public_key=${BuildConfig.PUBLIC_KEY}")
    fun listPaymentMethods(): Observable<List<PaymentMethodResponse>>

    @GET("payment_methods/card_issuers?public_key=${BuildConfig.PUBLIC_KEY}")
    fun listBanks(@Query("payment_method_id")
                  paymentMethodId: String): Observable<List<Bank>>

    @GET("payment_methods/installments?public_key=${BuildConfig.PUBLIC_KEY}")
    fun getInstallments(@Query("amount") amount: BigDecimal,
                        @Query("payment_method_id") paymentMethodId: String,
                        @Query("issuer.id") issuerId: String):
            Observable<List<InstallmentsResponse>>

}