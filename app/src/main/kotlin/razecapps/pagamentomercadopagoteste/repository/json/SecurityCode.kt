package razecapps.pagamentomercadopagoteste.repository.json

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SecurityCode(
        @SerializedName("length") val length: Int,
        @SerializedName("card_location") val cardLocation: String,
        @SerializedName("mode") val mode: String
) : Parcelable