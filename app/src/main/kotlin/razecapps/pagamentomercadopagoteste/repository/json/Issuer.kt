package razecapps.pagamentomercadopagoteste.repository.json

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Issuer(
        @SerializedName("id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("secure_thumbnail") val secureThumbnail: String,
        @SerializedName("thumbnail") val thumbnail: String
) : Parcelable