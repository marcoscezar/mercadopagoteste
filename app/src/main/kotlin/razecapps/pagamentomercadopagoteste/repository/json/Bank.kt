package razecapps.pagamentomercadopagoteste.repository.json

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Bank(
        @SerializedName("id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("secure_thumbnail") val secureThumbnail: String,
        @SerializedName("thumbnail") val thumbnail: String,
        @SerializedName("processing_mode") val processingMode: String,
        @SerializedName("merchant_account_id") val merchantAccountId: Long
) : Parcelable