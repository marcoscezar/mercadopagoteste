package razecapps.pagamentomercadopagoteste.repository.json

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Parcelize
data class PayerCost(
        @SerializedName("installments") val installments: Int,
        @SerializedName("installment_rate") val installmentRate: BigDecimal,
        @SerializedName("discount_rate") val discountRate: Int,
        @SerializedName("labels") val labels: List<String>,
        @SerializedName("installment_rate_collector") val installmentRateCollector: List<String>,
        @SerializedName("min_allowed_amount") val minAllowedAmount: Int,
        @SerializedName("max_allowed_amount") val maxAllowedAmount: Int,
        @SerializedName("recommended_message") val recommendedMessage: String,
        @SerializedName("installment_amount") val installmentAmount: BigDecimal,
        @SerializedName("total_amount") val totalAmount: BigDecimal
) : Parcelable