package razecapps.pagamentomercadopagoteste.repository.json

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Settings(
        @SerializedName("card_number") val cardNumber: CardNumber,
        @SerializedName("bin") val bin: Bin,
        @SerializedName("security_code") val securityCode: SecurityCode
) : Parcelable