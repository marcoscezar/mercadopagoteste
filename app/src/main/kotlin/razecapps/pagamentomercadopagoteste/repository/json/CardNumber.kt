package razecapps.pagamentomercadopagoteste.repository.json

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CardNumber(
        @SerializedName("validation") val validation: String,
        @SerializedName("length") val length: Int
) : Parcelable