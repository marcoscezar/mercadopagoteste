package razecapps.pagamentomercadopagoteste.repository.json

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InstallmentsResponse(
        @SerializedName("payment_method_id") val paymentMethodId: String,
        @SerializedName("payment_type_id") val paymentTypeId: String,
        @SerializedName("issuer") val issuer: Issuer,
        @SerializedName("processing_mode") val processingMode: String,
        @SerializedName("merchant_account_id") val merchantAccountId: Long,
        @SerializedName("payer_costs") val payerCosts: List<PayerCost>
) : Parcelable