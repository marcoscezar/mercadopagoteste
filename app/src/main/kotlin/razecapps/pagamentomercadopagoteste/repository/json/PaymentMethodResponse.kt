package razecapps.pagamentomercadopagoteste.repository.json

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentMethodResponse(
        @SerializedName("id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("payment_type_id") val paymentTypeId: String,
        @SerializedName("status") val status: String,
        @SerializedName("secure_thumbnail") val secureThumbnail: String,
        @SerializedName("thumbnail") val thumbnail: String,
        @SerializedName("deferred_capture") val deferredCapture: String,
        @SerializedName("settings") val settings: List<Settings>,
        @SerializedName("additional_info_needed") val additionalInfoNeeded: List<String>,
        @SerializedName("min_allowed_amount") val minAllowedAmount: Double,
        @SerializedName("max_allowed_amount") val maxAllowedAmount: Int,
        @SerializedName("accreditation_time") val accreditationTime: Int,
        @SerializedName("financial_institutions") val financialInstitutions: List<String>,
        @SerializedName("processing_modes") val processingModes: List<String>
) : Parcelable