package razecapps.pagamentomercadopagoteste.repository.json

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Bin(
        @SerializedName("pattern") val pattern: String,
        @SerializedName("installments_pattern") val installmentsPattern: String,
        @SerializedName("exclusion_pattern") val exclusionPattern: String
) : Parcelable