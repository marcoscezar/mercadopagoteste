package razecapps.pagamentomercadopagoteste.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.Toolbar
import android.text.SpannableStringBuilder
import android.view.View
import kotlinx.android.synthetic.main.activity_installments.*
import kotlinx.android.synthetic.main.linear_payment_value.*
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import razecapps.pagamentomercadopagoteste.R
import razecapps.pagamentomercadopagoteste.base.BaseActivity
import razecapps.pagamentomercadopagoteste.model.SelectableItemData
import razecapps.pagamentomercadopagoteste.repository.json.InstallmentsResponse
import razecapps.pagamentomercadopagoteste.ui.adapter.InstallmentsAdapter
import razecapps.pagamentomercadopagoteste.ui.presenter.InstallmentsContract
import razecapps.pagamentomercadopagoteste.util.createLinearRecyclerView
import razecapps.pagamentomercadopagoteste.util.formatToDollar
import java.math.BigDecimal

class InstallmentsActivity : BaseActivity(),
        InstallmentsContract.InstallmentsView,
        InstallmentsAdapter.OnSelectInstallmentListener {

    var bankSelected: SelectableItemData? = null
        get() = intent.getParcelableExtra(SelectBankActivity.BANK_SELECTED_KEY)

    var paymentMethod: SelectableItemData? = null
        get() = intent.getParcelableExtra(SelectPaymentMethodActivity.PAYMENT_METHOD_KEY)

    var inputAmount: BigDecimal? = null
        get() = intent.getSerializableExtra(MainActivity.PAY_VALUE_KEY) as BigDecimal?

    val installmentsPresenter: InstallmentsContract
    .InstallmentsPresenter<InstallmentsContract.InstallmentsView> by inject()

    companion object {
        const val SELECTED_INSTALLMENTS_KEY = "razecapps.selectedInstallmentsKey"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installments)

        setupToolbarTitle(toolbarInstallments as Toolbar,
                getString(R.string.text_installments_title))

        configureToolbarBackButton()

        textPaymentValueData.text =
                SpannableStringBuilder.valueOf(inputAmount?.formatToDollar())

        installmentsPresenter.setView(this)

        installmentsPresenter.listInstallments(inputAmount,
                    paymentMethod?.id, bankSelected?.id)
    }


    override fun showLoading() {
        frameSelectInstallmentsProgress.visibility = View.VISIBLE
        linearInstallmentsContent.visibility = View.GONE
    }

    override fun hideLoading() {
        frameSelectInstallmentsProgress.visibility = View.GONE
        linearInstallmentsContent.visibility = View.VISIBLE
    }

    override fun showError() {
        toast(getString(R.string.text_network_error))
    }

    override fun showInstallments(installments: List<InstallmentsResponse>) {
        recyclerInstallmentsList.createLinearRecyclerView(this,
                InstallmentsAdapter(installments.first().payerCosts.map { it.recommendedMessage })
                        .apply {
                    this.installmentListener = this@InstallmentsActivity
                })
    }

    override fun onInstallmentSelected(installmentSelected: String) {
        LocalBroadcastManager.getInstance(this)
                .sendBroadcast(Intent(BaseActivity.CLOSE_INTERMEDIARY_RECEIVER))

        val showSelectedPayIntent = Intent(MainActivity.FINISHED_PAY_RECEIVER)
        showSelectedPayIntent.putExtra(MainActivity.PAY_VALUE_KEY, inputAmount)
        showSelectedPayIntent.putExtra(SelectPaymentMethodActivity.PAYMENT_METHOD_KEY, paymentMethod)
        showSelectedPayIntent.putExtra(SelectBankActivity.BANK_SELECTED_KEY, bankSelected)
        showSelectedPayIntent.putExtra(SELECTED_INSTALLMENTS_KEY, installmentSelected)

        LocalBroadcastManager.getInstance(this).sendBroadcast(showSelectedPayIntent)

        finish()
    }

}