package razecapps.pagamentomercadopagoteste.ui.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.Toolbar
import android.text.SpannableStringBuilder
import android.view.View
import kotlinx.android.synthetic.main.activity_bank_select.*
import kotlinx.android.synthetic.main.linear_payment_value.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import razecapps.pagamentomercadopagoteste.R
import razecapps.pagamentomercadopagoteste.base.BaseActivity
import razecapps.pagamentomercadopagoteste.repository.json.Bank
import razecapps.pagamentomercadopagoteste.ui.adapter.ItemSelectableAdapter
import razecapps.pagamentomercadopagoteste.model.SelectableItemData
import razecapps.pagamentomercadopagoteste.ui.presenter.BanksContract
import razecapps.pagamentomercadopagoteste.util.createLinearRecyclerView
import razecapps.pagamentomercadopagoteste.util.formatToDollar
import java.math.BigDecimal

class SelectBankActivity : BaseActivity(), BanksContract.BanksView,
        ItemSelectableAdapter.OnPaymentSelectListener {

    var paymentValue: BigDecimal? = null
        get() = intent.getSerializableExtra(MainActivity.PAY_VALUE_KEY) as BigDecimal?

    var paymentMethod: SelectableItemData? = null
        get() = intent.getParcelableExtra(SelectPaymentMethodActivity.PAYMENT_METHOD_KEY)

    val presenter: BanksContract.BanksPresenter<BanksContract.BanksView> by inject()

    companion object {
        const val BANK_SELECTED_KEY = "razecapps.bankSelected"
    }


    val closeBroadcastReceiver: BroadcastReceiver = object: BroadcastReceiver() {

        override fun onReceive(context: Context?, intentReceived: Intent?) {
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_select)

        setupToolbarTitle(toolbarBankSelect as Toolbar,
                getString(R.string.text_select_bank_title))
        configureToolbarBackButton()

        textPaymentValueData.text = SpannableStringBuilder
                .valueOf(paymentValue?.formatToDollar())

        presenter.setView(this)

        paymentMethod?.run {
            presenter.listBanks(this.id)
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(closeBroadcastReceiver,
                IntentFilter(BaseActivity.CLOSE_INTERMEDIARY_RECEIVER))
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(closeBroadcastReceiver)
    }

    override fun showLoading() {
        frameSelectBankProgress.visibility = View.VISIBLE
        linearBanksContent.visibility = View.GONE
        constraintNoBanks.visibility = View.GONE
    }

    override fun hideLoading() {
        frameSelectBankProgress.visibility = View.GONE
        linearBanksContent.visibility = View.VISIBLE
    }

    override fun showError() {
        toast(getString(R.string.text_network_error))
    }

    override fun listBanks(banksList: List<Bank>) {
        recyclerBanksList.createLinearRecyclerView(this, ItemSelectableAdapter(banksList.map {
            SelectableItemData(it.id, it.thumbnail, it.name)
        }).apply {
            this.onPaymentSelectListener = this@SelectBankActivity
        })
    }


    override fun onPaymentSelect(selectableItemData: SelectableItemData) {
        startActivity<InstallmentsActivity>(BANK_SELECTED_KEY to selectableItemData,
                SelectPaymentMethodActivity.PAYMENT_METHOD_KEY to paymentMethod,
                MainActivity.PAY_VALUE_KEY to paymentValue)
    }

    override fun showEmptyBanks() {
        frameSelectBankProgress.visibility = View.GONE
        linearBanksContent.visibility = View.GONE
        constraintNoBanks.visibility = View.VISIBLE
    }

}