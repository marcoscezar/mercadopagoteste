package razecapps.pagamentomercadopagoteste.ui.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.Toolbar
import android.text.SpannableStringBuilder
import android.view.View
import kotlinx.android.synthetic.main.activity_method_payment_select.*
import kotlinx.android.synthetic.main.linear_payment_value.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import razecapps.pagamentomercadopagoteste.R
import razecapps.pagamentomercadopagoteste.base.BaseActivity
import razecapps.pagamentomercadopagoteste.model.SelectableItemData
import razecapps.pagamentomercadopagoteste.repository.json.PaymentMethodResponse
import razecapps.pagamentomercadopagoteste.ui.adapter.ItemSelectableAdapter
import razecapps.pagamentomercadopagoteste.ui.presenter.SelectPaymentContract
import razecapps.pagamentomercadopagoteste.util.createLinearRecyclerView
import razecapps.pagamentomercadopagoteste.util.formatToDollar
import java.math.BigDecimal

class SelectPaymentMethodActivity : BaseActivity(),
        SelectPaymentContract.SelectPaymentMethodView,
        ItemSelectableAdapter.OnPaymentSelectListener {

    val presenter by inject<SelectPaymentContract
    .SelectPaymentPresenter<SelectPaymentContract.SelectPaymentMethodView>>()

    var inputPaymentValue: BigDecimal? = null
            get() = intent?.getSerializableExtra(MainActivity.PAY_VALUE_KEY) as BigDecimal?


    companion object {

        const val PAYMENT_METHOD_KEY = "razecapps.paymentMethod"
    }

    val closeBroadcastReceiver: BroadcastReceiver = object: BroadcastReceiver() {

        override fun onReceive(context: Context?, intentReceived: Intent?) {
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_method_payment_select)

        setupToolbarTitle(paymentMethodToolbar as Toolbar,
                getString(R.string.text_select_payment_method))

        presenter.setView(this)

        textPaymentValueData.text = SpannableStringBuilder
                .valueOf(inputPaymentValue?.formatToDollar())
        configureToolbarBackButton()

        presenter.listPaymentMethods()

        LocalBroadcastManager.getInstance(this).registerReceiver(closeBroadcastReceiver,
                IntentFilter(BaseActivity.CLOSE_INTERMEDIARY_RECEIVER))
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(closeBroadcastReceiver)
    }

    override fun showLoading() {
        frameSelectPaymentProgress.visibility = View.VISIBLE
        linearPaymentMethods.visibility = View.GONE
    }

    override fun hideLoading() {
        frameSelectPaymentProgress.visibility = View.GONE
        linearPaymentMethods.visibility = View.VISIBLE
    }

    override fun showError() {
        toast(getString(R.string.text_error_payment_methods_load))
        frameSelectPaymentProgress.visibility = View.GONE
        linearPaymentMethods.visibility = View.VISIBLE
    }

    override fun showPaymentMethods(paymentMethods: List<PaymentMethodResponse>) {

        recyclerPaymentMethodsList.createLinearRecyclerView(this,
                ItemSelectableAdapter(paymentMethods.map {
            SelectableItemData(it.id, it.thumbnail, it.name)
        }).apply {
            this.onPaymentSelectListener = this@SelectPaymentMethodActivity
        })

    }

    override fun onPaymentSelect(selectableItemData: SelectableItemData) {
        startActivity<SelectBankActivity>(MainActivity.PAY_VALUE_KEY to inputPaymentValue,
                PAYMENT_METHOD_KEY to selectableItemData)
    }
}