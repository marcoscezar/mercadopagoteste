package razecapps.pagamentomercadopagoteste.ui.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import br.com.concrete.canarinho.watcher.ValorMonetarioWatcher
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import razecapps.pagamentomercadopagoteste.R
import razecapps.pagamentomercadopagoteste.base.BaseActivity
import razecapps.pagamentomercadopagoteste.model.SelectableItemData
import razecapps.pagamentomercadopagoteste.ui.presenter.MainContract
import razecapps.pagamentomercadopagoteste.util.parseFromDollar
import timber.log.Timber
import java.math.BigDecimal

class MainActivity : BaseActivity(), MainContract.MainView {

    private val presenter: MainContract.MainPresenter<MainContract.MainView> by inject()

    companion object {
        const val PAY_VALUE_KEY = "razecapps.pagamentomercadopagoteste.payValue"
        const val FINISHED_PAY_RECEIVER = "razecapps.finishedPayReceiver"
    }

    val showPaymentReceiver = object: BroadcastReceiver() {

        override fun onReceive(content: Context?, intentReceived: Intent?) {
            intentReceived?.run {
                val amount = this.getSerializableExtra(MainActivity.PAY_VALUE_KEY)
                val paymentMethod = this.getParcelableExtra<SelectableItemData>(SelectPaymentMethodActivity.PAYMENT_METHOD_KEY)
                val bankSelected = this.getParcelableExtra<SelectableItemData>(SelectBankActivity.BANK_SELECTED_KEY)
                val installmentSelected = this.getStringExtra(InstallmentsActivity.SELECTED_INSTALLMENTS_KEY)


                longToast(getString(R.string.text_finalization_pay, amount,
                        paymentMethod.id, bankSelected.textContent, installmentSelected))
            }

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupToolbarTitle(mainToolbar as Toolbar,
                getString(R.string.app_name))

        textInputEditValue.addTextChangedListener(ValorMonetarioWatcher.Builder()
                .comSimboloReal()
                .comMantemZerosAoLimpar().build())

        presenter.setView(this)

        setupPayButton()

        LocalBroadcastManager.getInstance(this).registerReceiver(showPaymentReceiver,
                IntentFilter(FINISHED_PAY_RECEIVER))
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(showPaymentReceiver)
    }

    private fun setupPayButton() {
        buttonPay.setOnClickListener {

            Timber.d("Veja o valor: ${textInputEditValue.text}")

            if (!TextUtils.isEmpty(textInputEditValue.text)) {
                val numericValue = textInputEditValue.text.toString().parseFromDollar()

                presenter.startPayProcess(numericValue)
            } else {
                showBlankMessage()
            }

        }
    }

    private fun showBlankMessage() {
        textInputEditValue.error = getString(R.string.text_input_blank_message)
    }

    override fun startPayProcess(value: BigDecimal) {
        startActivity<SelectPaymentMethodActivity>(PAY_VALUE_KEY to value)
    }

    override fun showInputError() {
        toast(getString(R.string.text_input_invalid_value))
    }
}
