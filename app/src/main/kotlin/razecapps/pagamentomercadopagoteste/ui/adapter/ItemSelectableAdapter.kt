package razecapps.pagamentomercadopagoteste.ui.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import razecapps.pagamentomercadopagoteste.R
import razecapps.pagamentomercadopagoteste.config.GlideApp
import razecapps.pagamentomercadopagoteste.model.SelectableItemData

class ItemSelectableAdapter(private val items: List<SelectableItemData>) :
        RecyclerView.Adapter<SelectableItemViewHolder>() {

    var onPaymentSelectListener: OnPaymentSelectListener? = null

    interface OnPaymentSelectListener {
        fun onPaymentSelect(selectableItemData: SelectableItemData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectableItemViewHolder {
        return SelectableItemViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_selectable, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SelectableItemViewHolder, position: Int) {
        holder.bind(items[position], this)
    }
}

class SelectableItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(currentItemData: SelectableItemData,
             adapter: ItemSelectableAdapter) {

        val paymentMethodName = view.findViewById<TextView>(R.id.textContentName)
        val constraintItemPaymentMethod = view
                .findViewById<ConstraintLayout>(R.id.constraintItemPaymentMethod)

        paymentMethodName.text = currentItemData.textContent

        GlideApp.with(view).load(currentItemData.urlThumb).fitCenter()
                .into(view.findViewById(R.id.imageContentLogo))

        constraintItemPaymentMethod.setOnClickListener {

            adapter.onPaymentSelectListener?.run {
                this.onPaymentSelect(currentItemData)
            }
        }
    }

}