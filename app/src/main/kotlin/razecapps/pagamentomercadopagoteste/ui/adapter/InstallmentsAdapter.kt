package razecapps.pagamentomercadopagoteste.ui.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import razecapps.pagamentomercadopagoteste.R

class InstallmentsAdapter(private val installmentItems: List<String>) :
        RecyclerView.Adapter<SingleItemViewHolder>() {

    var installmentListener: OnSelectInstallmentListener? = null

    interface OnSelectInstallmentListener {
        fun onInstallmentSelected(installmentSelected: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SingleItemViewHolder =
            SingleItemViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_installments, parent, false))

    override fun getItemCount(): Int = installmentItems.size

    override fun onBindViewHolder(holder: SingleItemViewHolder, position: Int) {
        holder.bind(installmentItems[position], this)
    }
}

class SingleItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(content: String, installmentsAdapter: InstallmentsAdapter) {

        val installmentsContent = view.findViewById<TextView>(R.id.textItemInstallmentsContent)
        installmentsContent.text = SpannableStringBuilder.valueOf(content)

        val constraintItemInstallment = view
                .findViewById<ConstraintLayout>(R.id.constraintItemInstallment)

        constraintItemInstallment.setOnClickListener {
            installmentsAdapter.installmentListener?.onInstallmentSelected(content)
        }
    }
}