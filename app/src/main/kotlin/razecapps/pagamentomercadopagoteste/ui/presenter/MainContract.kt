package razecapps.pagamentomercadopagoteste.ui.presenter

import java.math.BigDecimal

interface MainContract {

    interface MainView {
        fun startPayProcess(value: BigDecimal)
        fun showInputError()
    }

    interface MainPresenter<T> {
        fun startPayProcess(value: Number)
        fun setView(view: T)
    }

}