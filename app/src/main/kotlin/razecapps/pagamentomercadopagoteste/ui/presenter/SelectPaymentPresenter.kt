package razecapps.pagamentomercadopagoteste.ui.presenter

import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import razecapps.pagamentomercadopagoteste.repository.PaidMarketRepository
import timber.log.Timber

class SelectPaymentPresenter(private val paidMarketRepository: PaidMarketRepository,
                             private val subscribeScheduler: Scheduler,
                             private val observeScheduler: Scheduler) :
        SelectPaymentContract.SelectPaymentPresenter<SelectPaymentContract.SelectPaymentMethodView> {

    private val compositeDisposable = CompositeDisposable()

    var selectPaymentView: SelectPaymentContract.SelectPaymentMethodView? = null

    companion object {
        const val PAYMENT_TYPE = "credit_card"
    }

    override fun setView(view: SelectPaymentContract.SelectPaymentMethodView) {
        this.selectPaymentView = view
    }

    override fun listPaymentMethods() {
        compositeDisposable.add(paidMarketRepository.listPaymentMethods()
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
                .doOnSubscribe {
                    selectPaymentView?.showLoading()
                }.doAfterTerminate {
                    selectPaymentView?.hideLoading()
                }
                .subscribe({ paymentMethods ->

                    selectPaymentView?.showPaymentMethods(paymentMethods.filter {
                        it.paymentTypeId == PAYMENT_TYPE
                    })

                }, { error ->
                    Timber.d("$error")
                    selectPaymentView?.showError()
                }))
    }


}