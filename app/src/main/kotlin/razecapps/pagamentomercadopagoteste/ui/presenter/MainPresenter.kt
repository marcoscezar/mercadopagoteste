package razecapps.pagamentomercadopagoteste.ui.presenter

import java.math.BigDecimal
import java.math.RoundingMode

class MainPresenter : MainContract.MainPresenter<MainContract.MainView> {

    var mainView: MainContract.MainView? = null

    override fun setView(view: MainContract.MainView) {
        this.mainView = view
    }

    override fun startPayProcess(value: Number) {

        mainView?.run {

            if (value.toDouble().compareTo(BigDecimal
                            .ZERO.setScale(2, RoundingMode.CEILING).toDouble()) != 0) {
                this.startPayProcess(value.toDouble()
                        .toBigDecimal().setScale(2, java.math.RoundingMode.CEILING))
            } else {
                this.showInputError()
            }
        }

    }


}