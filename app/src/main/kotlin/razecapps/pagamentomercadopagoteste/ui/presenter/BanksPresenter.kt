package razecapps.pagamentomercadopagoteste.ui.presenter

import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import razecapps.pagamentomercadopagoteste.repository.PaidMarketRepository
import timber.log.Timber

class BanksPresenter(private val paidMarketRepository: PaidMarketRepository,
                     private val subscribeScheduler: Scheduler,
                     private val observeScheduler: Scheduler) :
        BanksContract.BanksPresenter<BanksContract.BanksView> {

    private val compositeDisposable = CompositeDisposable()

    var banksView: BanksContract.BanksView? = null

    override fun setView(view: BanksContract.BanksView) {
        this.banksView = view
    }

    override fun listBanks(paymentMethodId: String) {
        compositeDisposable.add(paidMarketRepository.listBanks(paymentMethodId)
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
                .doOnSubscribe {
                    banksView?.showLoading()
                }.doAfterTerminate {
                    banksView?.hideLoading()
                }
                .subscribe({ banks ->

                    if (banks.isNotEmpty()) {
                        banksView?.listBanks(banks)
                    } else {
                        banksView?.showEmptyBanks()
                    }

                }) { error ->
                    Timber.e("$error")
                    banksView?.showError()
                })
    }


}