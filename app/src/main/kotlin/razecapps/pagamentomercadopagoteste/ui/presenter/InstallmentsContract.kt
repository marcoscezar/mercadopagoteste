package razecapps.pagamentomercadopagoteste.ui.presenter

import razecapps.pagamentomercadopagoteste.repository.json.InstallmentsResponse
import java.math.BigDecimal

interface InstallmentsContract {

    interface InstallmentsView {
        fun showLoading()
        fun hideLoading()
        fun showError()
        fun showInstallments(installments: List<InstallmentsResponse>)
    }

    interface InstallmentsPresenter<T> {
        fun listInstallments(amount: BigDecimal?, paymentMethodId: String?, idIssuer: String?)
        fun setView(view: T)
    }

}