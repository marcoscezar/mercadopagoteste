package razecapps.pagamentomercadopagoteste.ui.presenter

import razecapps.pagamentomercadopagoteste.repository.json.Bank

interface BanksContract {

    interface BanksView {
        fun showLoading()
        fun hideLoading()
        fun showError()
        fun showEmptyBanks()
        fun listBanks(banksList: List<Bank>)
    }

    interface BanksPresenter<T> {
        fun listBanks(paymentMethodId: String)
        fun setView(view: T)
    }

}