package razecapps.pagamentomercadopagoteste.ui.presenter

import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import razecapps.pagamentomercadopagoteste.repository.PaidMarketRepository
import timber.log.Timber
import java.math.BigDecimal

class InstallmentsPresenter(private val paidMarketRepository: PaidMarketRepository,
                            private val subscribeScheduler: Scheduler,
                            private val observeScheduler: Scheduler) :
        InstallmentsContract.InstallmentsPresenter<InstallmentsContract.InstallmentsView> {

    var installmentsView: InstallmentsContract.InstallmentsView? = null

    private val compositeDisposable = CompositeDisposable()

    override fun listInstallments(amount: BigDecimal?,
                                  paymentMethodId: String?,
                                  idIssuer: String?) {
        if (amount != null
                && !paymentMethodId.isNullOrEmpty()
                && !idIssuer.isNullOrEmpty()) {
            compositeDisposable.add(paidMarketRepository
                    .getInstallments(amount, paymentMethodId!!, idIssuer!!)
                    .subscribeOn(subscribeScheduler)
                    .observeOn(observeScheduler)
                    .subscribe({ installments ->
                        installmentsView?.showInstallments(installments)
                    }, { error ->
                        Timber.e(error)
                    }))
        } else {
            installmentsView?.showError()
        }

    }

    override fun setView(view: InstallmentsContract.InstallmentsView) {
        this.installmentsView = view
    }

}