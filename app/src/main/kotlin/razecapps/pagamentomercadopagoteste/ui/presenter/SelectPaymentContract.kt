package razecapps.pagamentomercadopagoteste.ui.presenter

import razecapps.pagamentomercadopagoteste.repository.json.PaymentMethodResponse

interface SelectPaymentContract {

    interface SelectPaymentMethodView {
        fun showLoading()
        fun hideLoading()
        fun showError()
        fun showPaymentMethods(paymentMethods: List<PaymentMethodResponse>)
    }

    interface SelectPaymentPresenter<T> {
        fun listPaymentMethods()
        fun setView(view: T)
    }

}