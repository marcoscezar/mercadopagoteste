package razecapps.pagamentomercadopagoteste.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SelectableItemData(val id: String,
                              val urlThumb: String, val textContent: String): Parcelable