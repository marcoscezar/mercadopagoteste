package razecapps.pagamentomercadopagoteste.presenter

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.standalone.StandAloneContext.loadKoinModules
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import razecapps.pagamentomercadopagoteste.di.mockMainPresenter
import razecapps.pagamentomercadopagoteste.ui.presenter.MainContract
import razecapps.pagamentomercadopagoteste.ui.presenter.MainPresenter
import java.math.BigDecimal
import java.math.RoundingMode


class MainPresenterTest : KoinTest {

    private val presenter: MainPresenter by inject()

    @Mock
    private lateinit var view: MainContract.MainView

    @Before
    fun setupComponents() {

        MockitoAnnotations.initMocks(this)
        loadKoinModules(listOf(mockMainPresenter))

        presenter.setView(view)
    }


    @Test
    fun testStartPayProcess() {

        val inputMoney = BigDecimal(45.5).setScale(2, RoundingMode.CEILING)

        Assert.assertNotNull(presenter)

        presenter.startPayProcess(inputMoney)
        Mockito.verify(view).startPayProcess(inputMoney)
    }



}