package razecapps.pagamentomercadopagoteste.presenter

import io.reactivex.Observable
import io.reactivex.Scheduler
import org.junit.Before
import org.junit.Test
import org.koin.standalone.StandAloneContext.loadKoinModules
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import razecapps.pagamentomercadopagoteste.di.mockMainPresenter
import razecapps.pagamentomercadopagoteste.repository.PaidMarketRepository
import razecapps.pagamentomercadopagoteste.repository.json.PaymentMethodResponse
import razecapps.pagamentomercadopagoteste.ui.presenter.SelectPaymentContract
import razecapps.pagamentomercadopagoteste.ui.presenter.SelectPaymentPresenter

class SelectPaymentMethodPresenterTest : KoinTest {


    @Mock
    lateinit var paidMarketRepository: PaidMarketRepository

    lateinit var presenter: SelectPaymentPresenter

    @Mock
    lateinit var view: SelectPaymentContract.SelectPaymentMethodView

    val testScheduler: Scheduler by inject()

    @Before
    fun configureTest() {

        MockitoAnnotations.initMocks(this)

        loadKoinModules(listOf(mockMainPresenter))

        presenter = SelectPaymentPresenter(paidMarketRepository, testScheduler, testScheduler)
        presenter.setView(view)
    }


    @Test
    fun fetchPaymentMethodsListSuccessfully() {

        val responseList = mockList()

        doReturn(Observable.just(responseList)).`when`(paidMarketRepository).listPaymentMethods()

        presenter.listPaymentMethods()

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).showPaymentMethods(responseList)

    }



    @Test
    fun fetchPaymentMethodsListError() {

        doReturn(Observable.error<Throwable>(Throwable())).`when`(paidMarketRepository)
                .listPaymentMethods()

        presenter.listPaymentMethods()

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).showError()

    }


    private fun mockList(): List<PaymentMethodResponse> {
        return listOf(PaymentMethodResponse("visa",
                "visa",
                "credit_card",
                "",
                "",
                "",
                "",
                listOf(),
                listOf(),
                1000.0,
                2000,
                5,
                listOf(),
                listOf()))
    }


}