package razecapps.pagamentomercadopagoteste.di

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import razecapps.pagamentomercadopagoteste.ui.presenter.MainPresenter


val mockMainPresenter: Module = applicationContext {

    factory {
        MainPresenter()
    }

    factory {
        Schedulers.trampoline()
    }


}