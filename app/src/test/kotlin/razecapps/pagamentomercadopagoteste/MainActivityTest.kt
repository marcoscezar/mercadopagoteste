package razecapps.pagamentomercadopagoteste

import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import razecapps.pagamentomercadopagoteste.ui.activity.MainActivity

@RunWith(RobolectricTestRunner::class)
class MainActivityTest {

    @Test
    fun checkIfActivityStart() {
        assertNotNull(Robolectric.setupActivity(MainActivity::class.java))
    }
}
